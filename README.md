This is one demonstration implementation for sensors to send Signal K-formatted information over Wifi & MQTT to the SK receiver.

A short summary: 
This code is used to support a wifi-enabled GPS sensor (for deck mounting), which connects an [Adafruit Ultimate GPS](https://www.adafruit.com/product/746) (based on the MTK3339 chip) to an [Arduino MKR1000](https://www.arduino.cc/en/Main/ArduinoMKR1000) and a 1.2 Ah LiPo back-up battery. The code is not advanced, but it works. 
The same code is also used for an environmental sensor bank, combining a humidity, light level, temperature and air pressure sensor. 

The code uses the following key libraries:

- [PubSubClient](http://pubsubclient.knolleary.net/api.html#connect1) for MQTT, modified to handle larger messages (increase MQTT_MAX_PACKET_SIZE to 512 in PubSubClient.h)
- [ArduinoJson](https://github.com/bblanchon/ArduinoJson) for constructing the SK deltas
- [PString](http://arduiniana.org/libraries/pstring/) for handling the JSON string before sending. 
- [TinyGPS++](http://arduiniana.org/libraries/tinygpsplus/) for interpreting the GPS messages
- [SleepyDog](https://github.com/adafruit/Adafruit_SleepyDog/) providing the Watchdog timer to prevent a freeze in the PubSubClient library. 
- [WiFi101](https://github.com/arduino-libraries/WiFi101) for the wifi functionality
- wiring_private for accessing the on-board SERCOM (serial communications) hardware


The sensor module "sender" connects to a "receiver", a Raspberry Pi running OpenPlotter v0.19 (OP) in the following steps:

1. The sensor connects to the OP-instantiated Wifi access point, currently using hardcoded information (SSID, WPA password). 
2. The sensor connects to the OP-instantiated Mosquitto MQTT server, currently using hardcoded information (IP, port, MQTT username, MQTT password). Once I find a compatible mDNS discovery library, the IP and port can be autofound. [This one might work](https://github.com/arduino-libraries/ArduinoMDNS). 
3. The sensor subscribes to an MQTT channel, "gpsget", set up for this purpose. 
4. The sensor outputs SK deltas to this channel, updating one variable per message (five messages in total for five navigation keys: position.latitude, position.longitude, position.altitude, speedOverGround, and courseOverGroundTrue). Due to the design of ArduinoJson, I could not get it to put multiple key:value combinations in a single message. Each message is about 160 bytes long, and contains the following information (which adheres to the SK delta format as far as I could understand):

`{
	"updates": [{
		"source": {
			"timestamp": "2016-11-09T09:15:36Z",
			"$source": "deck.gps001"
		},
		"values": [{
			"path": "navigation.speedOverGround",
			"value": 0.8385444
		}]
	}]
}`

The timestamp is the time which the GPS returns, i.e. the time when the reported values were valid, and so lags a little behind real time. 

The messages are now received by OpenPlotter and added to the internal SK model. This can be used by, for example the NodeJS charting and graphing implementations. 

There is a power-saving feature: if the GPS does not have a valid fix, the Wifi disconnects. Every time before sending a message, it is checked if Wifi and MQTT are connected (otherwise it reconnects). As such, it uses on average about 50-60 mA at 5V (0.3W). This is reduced in stand-by situations by using the watchdog timer.

Tests show the sender to function for weeks when powered with external power. 

With a 1.2 Ah battery, the GPS sensor keeps transmitting positions for about 4-5 h. 
With the same battery, the environmental sensor bank keeps transmitting information for 2.5 days. 

The printable case (first version) looks like this. Grid squares are 10mm for reference:
![IMG_3191.png](https://bitbucket.org/repo/MoEpGo/images/4117487715-IMG_3191.png)
![IMG_3194.png](https://bitbucket.org/repo/MoEpGo/images/2916063005-IMG_3194.png)
![IMG_3192.png](https://bitbucket.org/repo/MoEpGo/images/1744557042-IMG_3192.png)