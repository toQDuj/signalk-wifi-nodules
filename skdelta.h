// A Signalk Delta generator for simple, single-source sensors. Inspired by, and borrowing from
// the similar Freeboard code by Robert Huitema robert@42.co.nz

#include <ArduinoJson.h>   // for constructing JSON messages
#include <PString.h>

namespace skdelta { // for clear separation
  
  typedef struct {
    char* timestamp;
    char* device;
  } sourceType;
  
  typedef struct {
    double value;
    char*  path;
    bool   _isUpdated;
  } doubleValueType;

  typedef struct {
    int value;
    char*  path;
    bool   _isUpdated;
  } longValueType;

  typedef struct {
    char* value;
    char* path;
    bool  _isUpdated;
  } timestampType;

  typedef struct {                  // general model
    sourceType source;              // new skdelta model
    doubleValueType latitude;       // degrees
    doubleValueType longitude;      // degrees
    doubleValueType altitude;       // meters
    doubleValueType sog;            // m/s
    doubleValueType cogt;           // radian
    doubleValueType roll;           // radian
    doubleValueType pitch;          // radian
    doubleValueType yaw;            // radian
    longValueType   numsatellites;  // integer value, stored as long, since the MKR1000 stores int as long anyway.
    doubleValueType hdop;           // horizontal dilution in decimal. (int32, converted to double)
    timestampType   datetime;       // ISO 8601 timestamp
    doubleValueType airTemperature; // Kelvin
    doubleValueType airPressure;    // Pascal
    doubleValueType airHumidity;    // fraction
    doubleValueType illuminance;    // lux
  } modelType;

  void initModel(modelType* m){
    m->source.device             = "deck.gps001";
    m->source.timestamp          = "2000-01-01T00:00:01.000Z";   
    m->latitude.path             = "navigation.position.latitude";   
    m->longitude.path            = "navigation.position.longitude"; 
    m->altitude.path             = "navigation.position.altitude";   
    m->sog.path                  = "navigation.speedOverGround";          
    m->cogt.path                 = "navigation.courseOverGroundTrue"; 
    m->numsatellites.path        = "navigation.gnss.satellites";
    m->hdop.path                 = "navigation.gnss.horizontalDilution";
    m->datetime.path             = "navigation.datetime";
    m->roll.path                 = "navigation.attitude.roll";
    m->pitch.path                = "navigation.attitude.pitch";
    m->yaw.path                  = "navigation.attitude.yaw";
    m->airTemperature.path       = "environment.outside.temperature"; 
    m->airPressure.path          = "environment.outside.pressure"; 
    m->airHumidity.path          = "environment.outside.humidity"; 
    m->illuminance.path          = "environment.outside.illuminance";    
    m->latitude._isUpdated       = false;
    m->longitude._isUpdated      = false;
    m->altitude._isUpdated       = false;
    m->sog._isUpdated            = false;
    m->cogt._isUpdated           = false;
    m->datetime._isUpdated       = false;
    m->airTemperature._isUpdated = false;
    m->airPressure._isUpdated    = false;
    m->airHumidity._isUpdated    = false;
    m->illuminance._isUpdated    = false;
    m->numsatellites._isUpdated  = false;
    m->hdop._isUpdated           = false;
    m->roll._isUpdated           = false;
    m->pitch._isUpdated          = false;
    m->yaw._isUpdated            = false;

  }
  
  void constructValue(JsonArray* baseArray, doubleValueType& fVal) { // appends a double (float) sensor value to the json object
    JsonObject& jval   = baseArray->createNestedObject();
    jval["path"]       = fVal.path;
    jval["value"]      = (double)fVal.value;
    //return baseArray;
  }

  void constructValue(JsonArray* baseArray, longValueType& fVal) { // appends a double (float) sensor value to the json object
    JsonObject& jval   = baseArray->createNestedObject();
    jval["path"]       = fVal.path;
    jval["value"]      = (long)fVal.value;
    //return baseArray;
  }
  
  void constructValue(JsonArray* baseArray, timestampType& fVal) { // appends a double (float) sensor value to the json object
    JsonObject& jval   = baseArray->createNestedObject();
    jval["path"]       = fVal.path;
    jval["value"]      = fVal.value;
    //return baseArray;
  }
 
template <typename T> void constructJson(PString& dest, sourceType source, T& fVal) { // values is an array of floatValueType
    // demo JSON object, takes about 264 bytes
    StaticJsonBuffer<1024> jsonBuffer; // 1 kByte should be enough for any signalK delta message
  
    JsonObject& jroot             = jsonBuffer.createObject();
      JsonArray& jupdates         = jroot.createNestedArray("updates");
        JsonObject& jsource1      = jupdates.createNestedObject(); // unnamed object
          jsource1["$source"]     = source.device;
          if (source.timestamp   != "2000-01-01T00:00:01.000Z"){ // has been updated
            jsource1["timestamp"] = source.timestamp;
          }
          JsonArray& jvalues      = jsource1.createNestedArray("values");
            // for (int vali = 0; vali < skval.numValues ; vali++) {
            constructValue (&jvalues, fVal);
            // }
    jroot.printTo(dest);
  }

  // yet unused, from the ArduinoJSON documentation:
  void mergeJson(JsonObject& dest, JsonObject& src) {
    for (auto kvp : src) {
      dest[kvp.key] = kvp.value;
    }
  }


}



