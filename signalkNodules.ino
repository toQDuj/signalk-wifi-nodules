// This code is intended to translate
// 1. GPS NMEA0183 information to a SignalK JSON delta, or 
// 2. An environmental sensor bank
// and send the information via MQTT to the server.
// Target microcontroller device is the Arduino MKR1000,
// the GPS used is Adafruit's "Ultimate GPS", a MTK3339 chip.
// The environmental board consists of a TSL2591 (Adafruit), a HTU21D (Sparkfun/Adafruit), BMP280 (Adafruit)
// The signalk on-board server is a Raspberry Pi 3 running openplotter v0.14
// Status: MQTT delta messages sent, interpreted by openplotter.
// v0.8: Robustifying...

#include "signalKNodules.h"      // main settings for every nodule

#define NODULE 1             // 1: "gps" or 2: "env"

#if NODULE == 1
#include "signalkGPSNodule.h"    // application-specific header file
#endif
#if NODULE == 2
#include "signalkENVNodule.h"
#endif

/// /// /// /// Initialisation code /// /// /// ///

void setup() {
  Serial.begin(115200);
  Serial.println(F("Signal-K Sender modules v0.9"));
  initSensor(); //  unit
  initWifi(); // Set up and connect to wifi
  initMQTT();
  lWaitMillis = millis() + wMils;  // initial setup of rollover-safe timer
}

/// /// /// /// Main loop code /// /// /// ///

void loop() {
  processSensor(); // Always process incoming GPS serial or necessary sensor messages
  
  if ( (long)( millis() - lWaitMillis ) >= 0) { // do every preset numer of seconds.
    Serial.print("in subloop");
    int startm = millis();
    if (sensorValid()) {
      Serial.println(" sensor values valid");
      if (WiFi.status() != WL_CONNECTED) WifiReconnect(); // Reconnect to AP if connection lost
      if (!client.loop()) MQTTreconnect();         // Reconnect to MQTT if connection lost
    
      // Update signalk delta model:
      updateSensor();

      // send sensor information.
      sendSensor();
      delay(5 - constrain((millis() - startm), 0, 5)); // to make sure we're not sending this more than once in the same loop
    }
    else {
      Serial.println(" sensor values invalid, waiting...");
      WifiDisconnect(); // save power
      delay(5 - constrain((millis() - startm), 0, 5)); // a bit of space
    }
    
    // lWaitMillis += wMils;
    lWaitMillis = millis() + wMils; // more robust for long loops.
  }
}

void sendSensor(){
  // only updated values are being sent.
      skdeltaSend(skModel.source, skModel.latitude);
      skdeltaSend(skModel.source, skModel.longitude);
      skdeltaSend(skModel.source, skModel.altitude);
      skdeltaSend(skModel.source, skModel.sog);
      skdeltaSend(skModel.source, skModel.cogt);
      skdeltaSend(skModel.source, skModel.numsatellites);
      skdeltaSend(skModel.source, skModel.hdop);
      skdeltaSend(skModel.source, skModel.pitch);
      skdeltaSend(skModel.source, skModel.roll);
      skdeltaSend(skModel.source, skModel.yaw);
      skdeltaSend(skModel.source, skModel.datetime);
      skdeltaSend(skModel.source, skModel.airTemperature);
      skdeltaSend(skModel.source, skModel.airPressure);
      skdeltaSend(skModel.source, skModel.airHumidity);
      skdeltaSend(skModel.source, skModel.illuminance);      
}

template <typename T> void skdeltaSend(skdelta::sourceType source, T& fVal) {
  if (fVal._isUpdated == false) return; // skippit if it is not updated.
  char skdeltaOut[512] ; // 400% larger to give some headroom
  PString ostring(skdeltaOut, sizeof(skdeltaOut));
  skdelta::constructJson(ostring, source, fVal) ;
  Serial.println(ostring);
  MQTTSend(ostring);
  fVal._isUpdated = false; // reset updated flag.
}


/// /// /// /// MQTT helper code /// /// /// ///

void initMQTT() {
  // sets up the MQTT connection with the server specified above.
  client.setServer(server, port);
  // client.setCallback(MQTTcallback);
  MQTTreconnect();
}

void MQTTreconnect() {
  delay(1000); // 1s delay for the connection to settle
  // Loop until we're reconnected
  int mTry = 0;
  while (!client.connected()) {
    mTry += 1;
    if (mTry > 5) {
      Serial.println("Tried to reconnect MQTT several times, resetting MKR1000...");
      Watchdog.enable(10); // pretty much forces an immediate reset
    }
    
    delay(50); // trying to get a cleaner flow by adding some settling time here and there.
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(MQTTClientID, MQTTusername, MQTTpassword)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc= ");
      Serial.print(client.state());
      Serial.println(" sleeping for 5 seconds"); 
      // Wait 5 seconds before retrying
      MQTTCleanup();
      delay(50);
      WifiDisconnect(); // seems to be necessary...
      Watchdog.sleep(5000); // Saving power..
      WifiReconnect(); // make sure we're still connected
    }
  }
}

void MQTTCleanup(){
  client.unsubscribe(MQTTtopic);
  client.disconnect();
}

void MQTTSend(PString mesg) {
  Serial.print("Sending SignalK Message of size:");
  Serial.println(mesg.length());
  if (!client.publish(MQTTtopic, mesg, false)) {
    Serial.println("Failed to publish SignalK message. ");
  }
}

void MQTTcallback(char* MQTTtopic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(MQTTtopic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

/// /// /// /// Wifi helper code /// /// /// ///
// From: https://create.arduino.cc/projecthub/nazarenko_a_v/mkr1000-connecting-to-the-wifi-3-steps-d02fed
// And: https://github.com/arduino-libraries/WiFi101/blob/master/examples/ConnectWithWPA/ConnectWithWPA.ino
// and a bit from myself (printHexBytes)

void initWifi() {
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }
  // Set low power mode:
  WiFi.lowPowerMode();
  // Print WiFi MAC address:
  printMacAddress();
  // scan for existing networks:
  Serial.println("Available Wifi networks...");
  listNetworks();
  WifiReconnect();
}

void WifiReconnect() { // (re-)connects to wifi
  int mTry = 0;
  while ( WiFi.status() != WL_CONNECTED) {
    mTry += 1;
    if (mTry > 5) {
      Serial.println("Tried to reconnect Wifi several times, resetting MKR1000...");
      Watchdog.enable(10); // pretty much forces an immediate reset
    }
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    if ( WiFi.begin(ssid, pass) != WL_CONNECTED) {
      Serial.println("Waiting 5 seconds before retrying wifi connection");
      WifiDisconnect();
      Watchdog.sleep(5000); // Saving power..
    }
    else
    {
      Serial.print("You're connected to the network");
      // set ip address and mac address
      ip = WiFi.localIP();
      WiFi.macAddress(mac);
      printCurrentNet();
      printWifiData();      
    }
  }
}

void WifiDisconnect() {
  Serial.println("Disconnecting Wifi to save power");
  WiFi.disconnect(); // for power saving
}

void printWifiData() {
  // print your WiFi shield's IP address:
  Serial.print("IP Address: ");
  Serial.println(ip);
  // print your MAC address:
  Serial.print("MAC address: ");
  printMacAddress();
}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print("BSSID: ");
  printHexBytes(bssid);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}

void printMacAddress() {
  // the MAC address of your Wifi shield
  Serial.print("MAC: ");
  printHexBytes(mac);
}

void printHexBytes(byte* sixByte) {
  // for printing mac addresses or other multi-byte hex numbers
  int bLen = sizeof(sixByte) / sizeof(byte);
  for (int bytei = bLen + 1; bytei > 0; bytei--) {
    Serial.print(sixByte[bytei], HEX);
    Serial.print(":");
  }
  Serial.println(sixByte[0], HEX);
}


void listNetworks() {
  // scan for nearby networks:
  Serial.println("** Scan Networks **");
  int numSsid = WiFi.scanNetworks();
  if (numSsid == -1)
  {
    Serial.println("Couldn't get a wifi connection");
    while (true);
  }

  // print the list of networks seen:
  Serial.print("number of available networks:");
  Serial.println(numSsid);

  // print the network number and name for each network found:
  for (int thisNet = 0; thisNet < numSsid; thisNet++) {
    Serial.print(thisNet);
    Serial.print(") ");
    Serial.print(WiFi.SSID(thisNet));
    Serial.print("\tSignal: ");
    Serial.print(WiFi.RSSI(thisNet));
    Serial.print(" dBm");
    Serial.print("\tEncryption: ");
    printEncryptionType(WiFi.encryptionType(thisNet));
    Serial.flush();
  }
}

void printEncryptionType(int thisType) {
  // read the encryption type and print out the name:
  switch (thisType) {
    case ENC_TYPE_WEP:
      Serial.println("WEP");
      break;
    case ENC_TYPE_TKIP:
      Serial.println("WPA");
      break;
    case ENC_TYPE_CCMP:
      Serial.println("WPA2");
      break;
    case ENC_TYPE_NONE:
      Serial.println("None");
      break;
    case ENC_TYPE_AUTO:
      Serial.println("Auto");
      break;
  }
}

