// holder for the MKR1000/battery/GPS nodule

tol = 0.01;
in_to_mm = 25.4; 


// total box height: 44

difference(){
    //base material:
    union(){
        translate([-5, 0, -3])
        cube([50, 72, 3 + tol]);
        cube([40, 72, 30]);
        translate([-tol, 48 - tol, 22 - tol])
        cube([40 + 2 * tol, 24, 17 + tol]);
    }
    // spare for battery:
    translate([2.0, -tol, 10.0])
    cube([36,71,10]);

    // spare for the GPS unit
    // board:
    translate([20 - 0.5025 * in_to_mm, -tol, 25]){
        // main PCB
        cube([1.03 * in_to_mm, 1.34 * in_to_mm, 2.2]);
        // extra space for SMD components
        translate([1, -tol, tol])
        cube([1.03 * in_to_mm - 2, 1.34 * in_to_mm - 4, 3.2]);
        // slot for GPS chip
        translate([3, -tol, tol])
        cube([17, 26, 7]);
        // slot for battery clip
        translate([.51 * in_to_mm - 10, -tol, -3.5 + tol])
        cube([20, 1.34 * in_to_mm, 3.5]);
        // ledge for cables
        translate([tol, -tol, -3])
        cube([1.02 * in_to_mm, 3, 6]);
        // plastic-saving:        
    }

    // spare for the IMU unit
    // board:
    translate([13.5 - 7.5, 72 - 20 - 4 - 2*  tol, 35]){
        // main PCB
        cube([27, 20.2, 2.2]);
        // extra space for SMD components
        translate([1, - tol, tol])
        cube([27 - 2, 20.2 - 3, 4.7]);
        // slot for cables
        translate([7 / 2, -tol, -5 + tol])
        cube([20, 5 + 2 * tol, 5]);
    }

    
    // plastic-saving:
    
    // mkr1000 space:
    translate([20 - 12.0, -tol, 5]){
        // main PCB
        cube([1.01 * 25, 61.5, 2.2]);
        // antenna spare at end
        translate([0.51 * 25 - 17 / 2, 61.5 - tol, -1.5 + tol])
        cube([17, 2.5 + tol, 2]);
        // sparing for wires and soldering connections
        translate([1.0, -tol, 2 + 1.5 - 14])
        cube([3, 61.5 + tol - 5, 14]);
        translate([1.02 * 25 - 4.0, -tol, 2 + 1.5 - 14])
        cube([3, 61.5 + tol - 5, 14]);        
        // sparing for wifi module
        translate([4 - tol, -tol, -10])
        cube([1.02 * 25 - 8 + 2 * tol, 62, 10 + tol]);
        // sparing for battery connector
        translate([-3 -tol - 10, -tol, -10])
        cube([20, 20, 10 + tol]);
    }
    // cube([50, 62, 50]);
}
