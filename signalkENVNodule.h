/// /// /// ENV-module-specific imports /// /// ///

#include <Wire.h>                // i2c everythings
#include <Adafruit_Sensor.h>     // They seem to love this one...
#include <Adafruit_BMP280.h>     // pressure sensor 0x77?
#include "SparkFunHTU21D.h"      // Humidity, compatible with MKR1000 0x40?
#include "Adafruit_TSL2591.h"    // Light sensor 0x29
// address 0x60 reserved by MKR1000
// #include "quaternionFilters.h"   // For 9-DOF IMU sensor
// #include "MPU9250.h"             // the 9-DOF IMU sensor library from Sparkfun

// int intPin = 6;  // interrupt pin for the IMU sensor. Not sure if necessary to use.
static unsigned long wMils = 5000 ; // delay for sending new sensor values to SK server .

// for MQTT: 
const char MQTTClientID[]    = "ENV001";  // the client ID

Adafruit_BMP280 bme; // I2C
HTU21D htu;
Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);

/// /// /// /// Supplementary comms code /// /// /// ///

void TSL2591PrintDetails() {
  /* Display the gain and integration time for reference sake */
  Serial.println("");
  Serial.println("------------------------------------");
  Serial.print  ("Gain:         ");
  tsl2591Gain_t gain = tsl.getGain();
  switch (gain)
  {
    case TSL2591_GAIN_LOW:
      Serial.println("1x (Low)");
      break;
    case TSL2591_GAIN_MED:
      Serial.println("25x (Medium)");
      break;
    case TSL2591_GAIN_HIGH:
      Serial.println("428x (High)");
      break;
    case TSL2591_GAIN_MAX:
      Serial.println("9876x (Max)");
      break;
  }
  Serial.print  ("Timing:       ");
  Serial.print((tsl.getTiming() + 1) * 100, DEC);
  Serial.println(" ms");
  Serial.println("------------------------------------");
  Serial.println("");
}


void configureSensor(void) // for TSL chip, needs pruning.
{
  tsl.begin();
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain

  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);  // longest integration time (dim light)
  TSL2591PrintDetails();

}

void initSensor(void) { // Set-up for the Environmental sensors
  bme.begin();
  htu.begin();
  configureSensor();
  skdelta::initModel(&skModel);

}

void updateSensorModel(skdelta::modelType* gm, double lux) {
  gm->source.device             = "deck.env001";
  gm->airTemperature.value      = (double)bme.readTemperature() + 274.15;
  gm->airTemperature._isUpdated = true;
  gm->airPressure.value         = (double)bme.readPressure();
  gm->airPressure._isUpdated    = true;
  gm->airHumidity.value         = (double)htu.readHumidity() / 100.0;
  gm->airHumidity._isUpdated    = true;
  if (lux >= 0.0) { // only if we have a valid reading
    gm->illuminance.value       = (double)lux;
    gm->illuminance._isUpdated  = true;
  }
}

float TSL2591CalculateLux(uint16_t ch0, uint16_t ch1) /*wbp*/
// adjusted function from the Adafruit library returning float instead of int
// based on https://forums.adafruit.us/viewtopic.php?f=19&t=59694
{
  float atime, again; /*wbp*/
  float    cpl, lux1, lux2, lux;

  // Check for overflow conditions first
  if ((ch0 == 0xFFFF) | (ch1 == 0xFFFF))
  {
    // Signal an overflow
    return -1.0;
  }

  // Note: This algorithm is based on preliminary coefficients
  // provided by AMS and may need to be updated in the future
  tsl2591IntegrationTime_t timing = tsl.getTiming();
  switch (timing)
  {
    case TSL2591_INTEGRATIONTIME_100MS :
      atime = 100.0F;
      break;
    case TSL2591_INTEGRATIONTIME_200MS :
      atime = 200.0F;
      break;
    case TSL2591_INTEGRATIONTIME_300MS :
      atime = 300.0F;
      break;
    case TSL2591_INTEGRATIONTIME_400MS :
      atime = 400.0F;
      break;
    case TSL2591_INTEGRATIONTIME_500MS :
      atime = 500.0F;
      break;
    case TSL2591_INTEGRATIONTIME_600MS :
      atime = 600.0F;
      break;
    default: // 100ms
      atime = 100.0F;
      break;
  }

  tsl2591Gain_t gain = tsl.getGain();
  switch (gain)
  {
    case TSL2591_GAIN_LOW :
      //      again = 1.0F;
      again = 1.03F; /*wbp*/
      break;
    case TSL2591_GAIN_MED :
      again = 25.0F;
      break;
    case TSL2591_GAIN_HIGH :
      //      again = 428.0F;
      again = 425.0F; /*wbp*/
      break;
    case TSL2591_GAIN_MAX :
      //      again = 9876.0F;
      again = 7850.0F; /*wbp*/
      break;
    default:
      again = 1.0F;
      break;
  }

  cpl = (atime * again) / TSL2591_LUX_DF;

  lux1 = ( (float)ch0 - (TSL2591_LUX_COEFB * (float)ch1) ) / cpl;
  lux2 = ( ( TSL2591_LUX_COEFC * (float)ch0 ) - ( TSL2591_LUX_COEFD * (float)ch1 ) ) / cpl;

  // The highest value is the approximate lux equivalent
  lux = lux1 > lux2 ? lux1 : lux2;

  return lux;  /*wbp*/
}


int TSL2591RangeAdjust(uint32_t lum) {
  // check whether the recorded value isn't over or under-loaded. +1 for underexposed, 0 is ok, -1 for overexposed
  // uint16_t ir = lum >> 16;
  uint16_t hyst = 25000;
  uint16_t full = lum & 0xFFFF;
  uint16_t ir = lum & 0xFFFF;
  uint16_t vis = lum & 0xFFFF - lum >> 16; // I care about the visible light.
  if (full <= 32760 - hyst) return +1; // underload, adjust positively
  if (full >= 32760 + hyst) return -1; // overload, adjust negatively
  if ((full >= 37860) && (full <= 37900) && (ir >= 37860) && (ir <= 37900)) return -1; // bug in sensor, overload at max.

  return 0;
}

bool TSL2591SenseAndAdjust(uint32_t lum) {
  // Performs a scan of the various gain and timing settings to get an ideal reading out. May take several iterations.

  // get gain and timing settings
  tsl2591Gain_t gain = tsl.getGain();
  tsl2591IntegrationTime_t timing = tsl.getTiming();

  // find out if we are outside the central measurement range
  int adjust = TSL2591RangeAdjust(lum);
  Serial.print  ("RangeAdjust:       ");
  Serial.println(adjust);
  int gainAdjust = 0; // in case we reached limits on timing adjustment, adjust gain

  if (adjust == 0) return true; // nothing to do..
  if ((adjust == -1) && (gain == TSL2591_GAIN_LOW) && (timing == TSL2591_INTEGRATIONTIME_100MS)) return false; // nothing to do, at max.
  if ((adjust == 1) && (gain == TSL2591_GAIN_MAX) && (timing == TSL2591_INTEGRATIONTIME_600MS)) return false; // nothing to do, at min.
  if (adjust == 1) // increase integration time
  {
    Serial.print("Adjusting light sensor timing up");
    switch (timing)
    {
      case TSL2591_INTEGRATIONTIME_100MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS); break;
      case TSL2591_INTEGRATIONTIME_200MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS); break;
      case TSL2591_INTEGRATIONTIME_300MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS); break;
      case TSL2591_INTEGRATIONTIME_400MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS); break;
      case TSL2591_INTEGRATIONTIME_500MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); break;
      case TSL2591_INTEGRATIONTIME_600MS:
        gainAdjust = 1; break;
    }
  }
  if (adjust == -1) // increase integration time
  {
    Serial.print("Adjusting light sensor timing down");
    switch (timing)
    {
      case TSL2591_INTEGRATIONTIME_100MS:
        gainAdjust = -1; break;
      case TSL2591_INTEGRATIONTIME_200MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); break;
      case TSL2591_INTEGRATIONTIME_300MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS); break;
      case TSL2591_INTEGRATIONTIME_400MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS); break;
      case TSL2591_INTEGRATIONTIME_500MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS); break;
      case TSL2591_INTEGRATIONTIME_600MS:
        tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS); break;
    }
  }
  Serial.print  ("GainAdjust:       ");
  Serial.println(gainAdjust);
  if (gainAdjust == 1) // increase integration time
  {
    Serial.print("Adjusting light sensor gain up");
    switch (gain)
    {
      case TSL2591_GAIN_LOW:
        tsl.setGain(TSL2591_GAIN_MED);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); break;
      case TSL2591_GAIN_MED:
        tsl.setGain(TSL2591_GAIN_HIGH);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); break;
      case TSL2591_GAIN_HIGH:
        tsl.setGain(TSL2591_GAIN_MAX);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS); break;
    }
  }
  if (gainAdjust == -1) // increase integration time
  {
    Serial.print("Adjusting light sensor gain down");
    switch (gain)
    {
      case TSL2591_GAIN_MED:
        tsl.setGain(TSL2591_GAIN_LOW);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); break;
      case TSL2591_GAIN_HIGH:
        tsl.setGain(TSL2591_GAIN_MED);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); break;
      case TSL2591_GAIN_MAX:
        tsl.setGain(TSL2591_GAIN_HIGH);
        tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS); break;
    }
  }

  TSL2591PrintDetails();

  return true;
}

void updateSensor() {
  // uint16_t lux = tsl.getLuminosity(TSL2591_FULLSPECTRUM);
  uint32_t lum = tsl.getFullLuminosity();
  TSL2591SenseAndAdjust(lum);
  lum = tsl.getFullLuminosity();
  int adj = TSL2591RangeAdjust(lum); // check if we are overloading
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  Serial.print("[ "); Serial.print(millis()); Serial.print(" ms ] ");
  Serial.print("IR: "); Serial.print(ir);  Serial.print("  ");
  Serial.print("Full: "); Serial.print(full); Serial.print("  ");
  Serial.print("Visible: "); Serial.print(full - ir); Serial.print("  ");
  Serial.print("Lux: "); Serial.println(TSL2591CalculateLux(full, ir));
  // Serial.print(lux); Serial.println(" lux");
  Serial.print(bme.readTemperature());
  Serial.println(" *C");

  Serial.print(bme.readPressure());
  Serial.println(" Pa");

  Serial.print("Temp: "); Serial.print(htu.readTemperature());
  Serial.print("\t\tHum: "); Serial.println(htu.readHumidity());
  if (adj == -1) { // overloading the sensor, do not updat
    Serial.print("light sensor overload, not updating light levels now...");
    updateSensorModel(&skModel, (double) - 1);
  }
  else updateSensorModel(&skModel, (double)TSL2591CalculateLux(full, ir));
}

void processSensor() {
  // nothing to process
}

bool sensorValid() {
  // more extensive tests whether the information supplied by the sensors is valid and up-to-date
  
  return ( true
         );
}
