/// /// /// GPS-module-specific imports /// /// /// 

#include <Arduino.h>             // required before wiring_private.h
#include <wiring_private.h>      // for using the SERCOM serial hardware
#include <Adafruit_BNO055.h>     // 9-DOF IMU sensor i2c address 0x28 or 0x29 (when addr-pin at 3V)
#include <TinyGPS++.h>           // extra functionality

static unsigned long wMils = 1 ; // delay for sending new sensor values to SK server .
// static unsigned long wMils = 500 ; // delay for sending new sensor values to SK server .

// for MQTT: 
const char MQTTClientID[]    = "GPS001";  // the client ID

sensors_event_t IMUEvent; // not sure if this can't be in init code.

// communications happens over a hardware serial port, since the MKR1000 has six(!).
// we just use a spare on the board, leaving the main serial port for debugging:
#define PIN_SERIAL2_RX       (1ul)                // Pin description number for PIO_SERCOM on D1
#define PIN_SERIAL2_TX       (0ul)                // Pin description number for PIO_SERCOM on D0
#define PAD_SERIAL2_TX       (UART_TX_PAD_0)      // SERCOM pad 0 TX
#define PAD_SERIAL2_RX       (SERCOM_RX_PAD_1)    // SERCOM pad 1 RX
Uart Serial2(&sercom3, PIN_SERIAL2_RX, PIN_SERIAL2_TX, PAD_SERIAL2_RX, PAD_SERIAL2_TX);

// TinyGPS++ settings
TinyGPSPlus   gpsp;                        // additional functionality
TinyGPSCustom NMEALat(gpsp, "GPRMC", 3);   // Extract raw latitude
TinyGPSCustom NMEALon(gpsp, "GPRMC", 5);   // Extract raw longitude
TinyGPSCustom NMEAValid(gpsp, "GPRMC", 2); // Extract GPS validity flag

// IMU initialisation:
Adafruit_BNO055 bno = Adafruit_BNO055();   // BNO055 with integrated motion processor

// "Ultimate GPS" settings copied from Adafruit_GPS.h:
#define PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ  "$PMTK220,5000*1B"  // Once every 5 seconds, 200 millihertz.
#define PMTK_API_SET_FIX_CTL_200_MILLIHERTZ  "$PMTK300,5000,0,0,0,0*18"  // Once every 5 seconds, 200 millihertz.
#define PMTK_SET_NMEA_UPDATE_1HZ             "$PMTK220,1000*1F"
#define PMTK_API_SET_FIX_CTL_1HZ             "$PMTK300,1000,0,0,0,0*1C"
#define PMTK_SET_BAUD_9600                   "$PMTK251,9600*17"
#define PMTK_SET_NMEA_OUTPUT_RMCGGA          "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28"
#define PMTK_SET_NMEA_OUTPUT_RMCONLY         "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29"

// Local flags:
#define GPSECHO true // Echoes GPS messages to arduino serial monitor

/// /// /// /// Extra Serial port helper code /// /// /// ///
void initSerial2() {
  // set up extra serial port for GPS comms.
  pinPeripheral(PIN_SERIAL2_TX, PIO_SERCOM);   // Assign pins 0 & 1 SERCOM functionality
  pinPeripheral(PIN_SERIAL2_RX, PIO_SERCOM);
  Serial2.begin(9600);           // Begin Serial2
}

void SERCOM3_Handler() {  // Interrupt handler for SERCOM3
  Serial2.IrqHandler();
}

/// /// /// /// sensor readout validators /// /// /// /// 
bool sensorValidGPS() {
  // more extensive tests whether the information supplied by the GPS is valid and up-to-date
  return (
           (gpsp.location.isValid()) &&     // from a complete sentence
           (gpsp.location.age() < 10000) && // not older than 10 seconds
           (NMEAValid.value()[0] == 'A')    // and from a GPRMC sentence with a valid fix
         );
}

bool sensorValidIMU() {
  uint8_t validSystem, validGyro, validAccel, validMag = 0;
  bno.getCalibration(&validSystem, &validGyro, &validAccel, &validMag);
  // at least the system value should be >0
  uint8_t system_status, self_test_results, system_error = 0;
  bno.getSystemStatus(&system_status, &self_test_results, &system_error);
  // at least the system_error should be 0x00 (no error)
  return ((validSystem > 0) && (system_error == 0));
  
}

bool sensorValid() {
  return !((sensorValidGPS() == false) && (sensorValidIMU() == false));
}

/// /// /// /// Supplementary GPS Comms code /// /// /// ///

void initSensor() { // Set-up for the Adafruit Ultimate GPS and IMU
  bno.begin(); 
  // BNO needs some time to start
  initSerial2();
  Serial2.println(PMTK_SET_BAUD_9600);
  Serial2.println(PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ); // once in five seconds is sufficient
  Serial2.println(PMTK_API_SET_FIX_CTL_200_MILLIHERTZ); // ibid.
  Serial2.println(PMTK_SET_NMEA_OUTPUT_RMCGGA);         // RMC and GGA messages
  skdelta::initModel(&skModel);

  bno.setExtCrystalUse(true);

}

void updateSensorModel(skdelta::modelType* gm, TinyGPSPlus* gp) {
      gm->source.device            = "deck.gps001";  
      if (sensorValidGPS()) { 
        char tsbuffer[60]; // get time. needs 27 bytes, but reserve a bit more for safety.
        sprintf(tsbuffer, "%d-%02d-%02dT%02d:%02d:%02d.000Z", 
          constrain((int)gp->date.year(), 1900, 2100),
          constrain((int)gp->date.month(), 1, 12),
          constrain((int)gp->date.day(), 1, 31),
          constrain((int)gp->time.hour(), 0, 24),
          constrain((int)gp->time.minute(), 0, 60),
          constrain((int)gp->time.second(), 0, 60));
        gm->source.timestamp         = tsbuffer;
        gm->datetime.value           = (char*)tsbuffer; 
        gm->datetime._isUpdated      = true;
        gm->latitude.value           = (double)gp->location.lat(); 
        gm->latitude._isUpdated      = true;
        gm->longitude.value          = (double)gp->location.lng();
        gm->longitude._isUpdated     = true;
        gm->altitude.value           = (double)gp->altitude.meters();
        gm->altitude._isUpdated      = true;
        gm->sog.value                = (double)gp->speed.mps();
        gm->sog._isUpdated           = true;
        gm->cogt.value               = (double)gp->course.deg() * 3.14159265 / 180.0; //CoG is in radian.
        gm->cogt._isUpdated          = true;
        gm->numsatellites.value      = (long)gp->satellites.value();
        gm->numsatellites._isUpdated = true;
        gm->hdop.value               = (double)gp->hdop.value() / 100.0; // hdop from TinyGPS++ comes in as long, in 1/100ths.
        gm->hdop._isUpdated          = true;
      }
      else{
        gm->source.timestamp         = "2000-01-01T00:00:01.000Z";       
      }

      if (sensorValidIMU()) {
        bno.getEvent(&IMUEvent);
        gm->pitch.value               = (double)IMUEvent.orientation.z / -180.0 * 3.14159265; // both of these are inverted..
        gm->roll.value                = (double)IMUEvent.orientation.y / -180.0 * 3.14159265; // ibid.
        gm->yaw.value                 = (double)IMUEvent.orientation.x / 180.0 * 3.14159265; // positive values only, please. 
        gm->roll._isUpdated           = true;
        gm->pitch._isUpdated          = true;
        gm->yaw._isUpdated            = true;
      }
}


void updateSensor(){
  updateSensorModel(&skModel, &gpsp);
}

void processSensor() {
  while (Serial2.available()) {
    char c = Serial2.read();
    if (GPSECHO) Serial.print(c);
    gpsp.encode(c);
  }
  
}




