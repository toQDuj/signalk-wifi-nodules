#include <WiFi101.h>             // for wireless
#include "PubSubClient.h"        // for MQTT messaging
#include "skdelta.h"             // A new signalk model for deltas
#include <PString.h>             // for putting the SignalK model in
#include <Adafruit_SleepyDog.h>  // Watchdog timer to avoid getting stuck with wifi reconnects

#define MQTT_MAX_PACKET_SIZE 512 // Extra Big.. 

// Wifi settings:
const char ssid[] = "SignalK";          // The signal K network
const char pass[] = "SigKIsKool";       // network password
int status        = WL_IDLE_STATUS;     // the Wifi radio's status
byte mac[6]       = { 0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED }; // local mac address (will be updated)
IPAddress ip(0, 0, 0, 0);               // local IP address (updated later)

// MQTT settings
const IPAddress server(10, 10, 10, 1);   // SignalK MQTT server IP address; can be autofound using mdns?
const int port = 1883 ;                  // MQTT server port
WiFiClient WClient;                      // client for making connections
PubSubClient client(WClient);            // client for MQTT connections
const char MQTTusername[] = "boat";      // username for MQTT connection
const char MQTTpassword[] = "hardcoded"; // hardcoded passwords suck, but proof-of-concept
const char MQTTtopic[]    = "SKDelta";  // the subscription topic

// for avoiding millis issues, from http://playground.arduino.cc/Code/TimingRollover
static unsigned long lWaitMillis;

// SignalK settings:
skdelta::modelType skModel;


